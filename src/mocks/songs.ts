import { Song } from '../components/SongTile';

export const songsData: Song[] = [
  {
    id: 'f1f9',
    title: 'Warriors',
    imageUrl: '',
    author: 'Imagine Dragons',
  },
  {
    id: 'ac4e',
    title: 'Poker Face',
    imageUrl: '',
    author: 'Lady Gaga',
  },
];
