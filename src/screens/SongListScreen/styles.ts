import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'space-between',
  },
  content: {
    paddingHorizontal: 8,
  },
  list: {
    paddingVertical: 8,
  },
  addButton: {
    alignSelf: 'center',
    marginTop: 16,
  },
});
