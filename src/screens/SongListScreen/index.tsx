import React, { FC } from 'react';
import { FlatList, SafeAreaView, View } from 'react-native';
import { Button, Divider, Text } from '@react-native-material/core';
import { Header } from '../../components/common/Header';
import { styles } from './styles';
import { SongTile } from '../../components/SongTile';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { songsData } from '../../mocks/songs';

interface SongListScreenProps {
  navigation: NativeStackNavigationProp<any>;
}

export const SongListScreen: FC<SongListScreenProps> = ({ navigation }) => {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View>
          <Header />
          <View style={styles.content}>
            <Text variant="h5">All tabs</Text>
            <FlatList
              data={songsData}
              renderItem={({ item }) => (
                <SongTile
                  item={item}
                  onPress={() => navigation.navigate('Song', { id: item.id })}
                />
              )}
              style={styles.list}
            />
          </View>
        </View>
        <View>
          <Divider />
          <Button title="＋Add Song 8" style={styles.addButton} />
        </View>
      </View>
    </SafeAreaView>
  );
};
