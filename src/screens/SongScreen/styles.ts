import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  // @ts-ignore
  image: width => ({
    width: '100%',
    height: width * 0.75,
  }),
  content: {
    padding: 16,
  },
});
