import React, { FC } from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  useWindowDimensions,
  View,
} from 'react-native';
import { Text } from '@react-native-material/core';
import { styles } from './styles';
import { songsData } from '../../mocks/songs';
import { RouteProp } from '@react-navigation/native';

interface SongScreenProps {
  route: RouteProp<Record<'id', { id: string }>>;
}

export const SongScreen: FC<SongScreenProps> = ({ route }) => {
  const { id } = route.params;
  const { width } = useWindowDimensions();

  const currentSong = songsData.find(song => song.id === id) || songsData[0];

  return (
    <SafeAreaView>
      <ScrollView>
        <Image
          // @ts-ignore
          style={styles.image(width)}
          source={require('../../assets/images/default.jpeg')}
        />
        <View style={styles.content}>
          <Text variant="h4">{currentSong.title}</Text>
          <Text>by {currentSong.author}</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
