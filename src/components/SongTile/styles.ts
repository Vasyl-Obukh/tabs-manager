import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    marginBottom: 8,
  },
  wrapper: {
    flexDirection: 'row',
    borderRadius: 4,
    overflow: 'hidden',
  },
  image: {
    height: 72,
    width: 96,
  },
  content: {
    padding: 8,
    justifyContent: 'space-between',
  },
  author: {
    fontStyle: 'italic',
  },
});
