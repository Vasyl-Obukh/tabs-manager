import React, { FC } from 'react';
import { Image, View } from 'react-native';
import { styles } from './styles';
import { Pressable, Surface, Text } from '@react-native-material/core';

export interface Song {
  id: string;
  title: string;
  imageUrl?: string;
  author?: string;
}

interface SongTileProps {
  item: Song;
  onPress: () => void;
}

export const SongTile: FC<SongTileProps> = ({ item, onPress }) => (
  <Surface elevation={2} category="medium" style={styles.container}>
    <Pressable style={styles.wrapper} onPress={onPress}>
      <Image
        style={styles.image}
        source={require('../../assets/images/default.jpeg')}
      />
      <View style={styles.content}>
        <Text variant="h6">{item.title}</Text>
        <Text>
          by <Text style={styles.author}>{item.author}</Text>
        </Text>
      </View>
    </Pressable>
  </Surface>
);
