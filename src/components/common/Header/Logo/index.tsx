import React, { FC } from 'react';
import { Image, View } from 'react-native';
import { Text } from '@react-native-material/core';
import { styles } from './styles';

export const Logo: FC = () => (
  <View style={styles.container}>
    <Image
      style={styles.icon}
      source={require('../../../../assets/images/logo.png')}
    />
    <Text variant="h6">Tabs manager</Text>
  </View>
);
