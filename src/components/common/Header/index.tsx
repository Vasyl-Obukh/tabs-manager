import React, { FC } from 'react';
import { View } from 'react-native';
import { Logo } from './Logo';

export const Header: FC = () => (
  <View>
    <Logo />
  </View>
);
