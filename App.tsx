import React, { FC } from 'react';
import { SongListScreen } from './src/screens/SongListScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { SongScreen } from './src/screens/SongScreen';

const Stack = createNativeStackNavigator();

const App: FC = () => (
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
        name="SongList"
        component={SongListScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Song"
        component={SongScreen}
        // options={{ headerShown: false }}
      />
    </Stack.Navigator>
  </NavigationContainer>
);

export default App;
